FROM node:12.18.1

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY package*.json ./
COPY yarn.lock ./
COPY gulpfile.js ./
COPY tsconfig.json ./
COPY tslint.json ./
COPY .travis.yml ./
COPY _config.yml ./
COPY .env ./
COPY .sequelizerc ./

#ENV DB_NAME sql_test_db
#ENV DB_USER SA
#ENV DB_PASS Pass1234@
#ENV DB_PORT 1433
#ENV DB_HOST 192.168.99.100
#ENV MAX_POOL 10
#ENV MIN_POOL 1

RUN yarn install
RUN yarn build

COPY ./build ./

EXPOSE 1337

CMD [ "npm", "run", "start:prod" ]
