Für die **API** ist im Prinzip nur eine Sache wichtig, und das ist der Authorization Code. Dieser wird in dem .env File abgelegt wie die .env.example vorzeigt

Für die Verbindung zur Datenbank muss unter src/sqlz/config/config.json die Korrekten Daten eingegeben werden.
Die Datenbank muss auch nach dem ersten Start etwas bearbeitet werden. Dazu liegt hier im root Verzeichnis ein .sql File. Danach sollten die Container neu gestartet werden.

Zuerst muss der Befehl `yarn build` oder `npm run build` ausgeführt werden, dann kann über über den Befehl `yarn docker:build` oder `npm run docker:build` kann das image der API gebaut werden.
