import * as NewsGet from './news.get'
import * as NewsPost from './news.post'
import * as NewsPut from './news.put'

export { NewsGet, NewsPost, NewsPut }
