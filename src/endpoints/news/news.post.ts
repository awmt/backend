import { Request, Response } from 'express'
import { NewsDao } from '../../dao/_index'

export function create(req: Request, res: Response) {
  const { authorization } = req.headers

  if (authorization !== process.env.AUTHORIZATION) {
    res.boom.badRequest('Bad authorization')
    res.end()
  }

  return NewsDao.create(req.body)
    .then(news => res.status(201).send(news))
    .catch(error => res.boom.badRequest(error))
}

export function findByDays(req: Request, res: Response) {
  return NewsDao.findByDays(req.body)
    .then(news => res.status(201).send(news))
    .catch(error => res.boom.badRequest(error))
}

export function lastOnes(req: Request, res: Response) {
  return NewsDao.lastOnes(req.body)
    .then(news => res.status(201).send(news))
    .catch(error => res.boom.badRequest(error))
}
