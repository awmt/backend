import {Request, Response} from 'express'
import {NewsDao} from '../../dao/_index'

export function update(req: Request, res: Response) {
  const {authorization} = req.headers

  if (authorization !== process.env.AUTHORIZATION) {
    res.boom.badRequest('Bad authorization')
    res.end()
  }

  return NewsDao.update(req.body)
    .then(news => res.status(200).send(news))
    .catch(error => res.boom.badRequest(error))
}
