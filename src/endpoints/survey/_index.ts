import * as SurveyGet from "./survey.get";
import * as SurveyPost from "./survey.post";
import * as SurveyPut from "./survey.put";

export { SurveyGet, SurveyPost, SurveyPut };
