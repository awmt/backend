import { Request, Response } from 'express'
import { SurveyDao } from "../../dao/_index";

export function create(req: Request, res: Response) {
  const { authorization } = req.headers

  if (authorization !== process.env.AUTHORIZATION) {
    res.boom.badRequest('Bad authorization')
    res.end()
  }

  return SurveyDao.create(req.body)
    .then(survey => res.status(201).send(survey))
    .catch(error => res.boom.badRequest(error))
}

export function withAnswers(req: Request, res: Response) {
  return SurveyDao.findWithAnswers(req.body)
    .then(survey => res.status(201).send(survey))
    .catch(error => res.boom.badRequest(error))
}
