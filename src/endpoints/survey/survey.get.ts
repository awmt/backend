import { Request, Response } from 'express'
import { SurveyDao } from "../../dao/_index";

export function list(req: Request, res: Response) {
  return SurveyDao
    .findAll()
    .then(survey => res.status(200).send(survey))
    .catch(error => res.boom.badRequest(error))
}

export async function getById(id: string) {
  const surveys = await SurveyDao.findAll();
  for (const key in surveys) {
    const entry = surveys[key];
    if (entry.id === id) return entry;
  }

  return null;
}
