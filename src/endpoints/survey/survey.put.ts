import { Request, Response } from 'express'
import { SurveyDao } from "../../dao/_index";

export function update(req: Request, res: Response) {
  const { authorization } = req.headers

  if (authorization !== process.env.AUTHORIZATION) {
    res.boom.badRequest('Bad authorization')
    res.end()
  }

  return SurveyDao.update(req.body)
    .then(survey => res.status(200).send(survey))
    .catch(error => res.boom.badRequest(error))
}
