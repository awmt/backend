import * as LanguageController from "./languages/_index";
import * as AppUserController from "./appusers/_index";
import * as NewsController from "./news/_index";
import * as SurveyController from "./survey/_index";
import * as AnswerController from "./answer/_index";
import * as ResultController from "./result/_index";

export { LanguageController, AppUserController, NewsController, SurveyController, AnswerController, ResultController };
