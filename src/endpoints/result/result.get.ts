import { Request, Response } from 'express'
import { ResultDao } from "../../dao/_index";

export function list(req: Request, res: Response) {
  return ResultDao
    .findAll()
    .then(result => res.status(200).send(result))
    .catch(error => res.boom.badRequest(error))
}

export async function getById(id: string) {
  const surveys = await ResultDao.findAll();
  for (const key in surveys) {
    const entry = surveys[key];
    if (entry.id === id) return entry;
  }

  return null;
}

export async function evaluation(req: Request, res: Response) {
  return ResultDao
    .currentResults()
    .then(result => res.status(200).send(result))
    .catch(error => res.boom.badRequest(error))
}
