import { Request, Response } from 'express'
import { ResultDao } from "../../dao/_index";

export function update(req: Request, res: Response) {
  const { authorization } = req.headers

  if (authorization !== process.env.AUTHORIZATION) {
    res.boom.badRequest('Bad authorization')
    res.end()
  }

  return ResultDao.update(req.body)
    .then(result => res.status(200).send(result))
    .catch(error => res.boom.badRequest(error))
}
