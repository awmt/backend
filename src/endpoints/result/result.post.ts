import { Request, Response } from 'express'
import { ResultDao } from "../../dao/_index";

export function create(req: Request, res: Response) {
  const { authorization } = req.headers

  if (authorization !== process.env.AUTHORIZATION) {
    res.boom.badRequest('Bad authorization')
    res.end()
  }

  return ResultDao.create(req.body)
    .then(result => res.status(201).send(result))
    .catch(error => res.boom.badRequest(error))
}

export function findBySurveyId(req: Request, res: Response) {
  return ResultDao.findBySurveyId(req.body)
    .then(result => res.status(201).send(result))
    .catch(error => res.boom.badRequest(error))
}
