import * as ResultGet from "./result.get"
import * as ResultPost from "./result.post"
import * as ResultPut from "./result.put"

export { ResultGet, ResultPost, ResultPut };
