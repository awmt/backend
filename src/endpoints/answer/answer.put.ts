import { Request, Response } from 'express'
import { AnswerDao } from "../../dao/_index";

export function update(req: Request, res: Response) {
  const { authorization } = req.headers

  if (authorization !== process.env.AUTHORIZATION) {
    res.boom.badRequest('Bad authorization')
    res.end()
  }

  return AnswerDao.update(req.body)
    .then(answer => res.status(200).send(answer))
    .catch(error => res.boom.badRequest(error))
}
