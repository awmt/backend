import * as AnswerGet from "./answer.get";
import * as AnswerPost from "./answer.post";
import * as AnswerPut from "./answer.put";

export { AnswerGet, AnswerPost, AnswerPut };
