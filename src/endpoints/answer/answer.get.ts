import { Request, Response } from 'express'
import { AnswerDao } from "../../dao/_index";

export function list(req: Request, res: Response) {
  return AnswerDao
    .findAll()
    .then(answer => {
      res.status(200).send(answer)
    })
    .catch(error => res.boom.badRequest(error))
}

export async function getBySurveyId(id: string) {
  const answers = await AnswerDao.findAll();
  let chosen = [];

  for (const key in answers) {
    const entry = answers[key].dataValues;
    if (entry.surveyId === id) chosen.push(entry);
  }

  return chosen;
}
