import { DATE, Model, STRING } from "sequelize";
import sequelize from "./_index";

export class Survey extends Model {

}

export class SurveyModel {
  id: string;
  question: string;
  createdAt: Date;
  updatedAt: Date;
  last_publication_date: Date;
}

Survey.init(
  {
    question: STRING(10000),
    last_publication_date: DATE
  },
  { sequelize, modelName: "Survey" },
);
