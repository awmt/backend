import { DATE, Model, STRING, UUID } from 'sequelize'
import sequelize from './_index'
import { Survey } from "./survey";

export class SurveyAnswer extends Model {

}

export class SurveyAnswerModel {
  id: string
  content: string
  createdAt: Date
  updatedAt: Date
}

SurveyAnswer.init(
  {
    content: STRING(255)
  },
  { sequelize, modelName: 'Survey_answer' }
)

SurveyAnswer.belongsTo(Survey, {
  as: 'survey'
})

// SurveyAnswer.hasMany(Result, {as: 'results'});
