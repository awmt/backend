import { DataTypes, Model } from "sequelize";
import sequelize from "./_index";
import { SurveyAnswer } from "./survey_answer";
import { Result } from "./result";

export class RelAnswerResult extends Model {

}

export class RelAnswerResultModel {
  createdAt: Date
  updatedAt: Date
}

RelAnswerResult.init({
  answerId: {
    type: DataTypes.STRING,
    primaryKey: true,
    allowNull: false
  },
  resultId: {
    type: DataTypes.STRING,
    primaryKey: true,
    allowNull: false,
  },
},
  { sequelize, modelName: 'rel_answer_result' }
)

RelAnswerResult.belongsTo(SurveyAnswer, {
  as: 'answer'
})

RelAnswerResult.belongsTo(Result, {
  as: 'result'
})
