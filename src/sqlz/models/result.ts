import { Model, STRING } from "sequelize";
import sequelize from "./_index";
import { Survey } from "./survey";
export class Result extends Model {

}

export class ResultModel {
  id: string;
  createdAt: Date;
  updatedAt: Date;
}

Result.init({

},
  { sequelize, modelName: "Result" });

Result.belongsTo(Survey, {
  as: 'survey'
})
