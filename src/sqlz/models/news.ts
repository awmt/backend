import {DATE, Model, STRING, UUID} from 'sequelize'
import sequelize from './_index'

export class News extends Model {
}

export class NewsModel {
  id: string
  h1: string | null
  h2: string | null
  image: string | null
  teaser: string | null
  url: string
  date: Date | null
  lastCrawled: Date
  createdAt: Date
  updatedAt: Date
}

News.init(
  {
    h1: STRING(255),
    h2: STRING(255),
    image: STRING(255),
    teaser: STRING,
    url: STRING(255),
    date: DATE,
    lastCrawled: DATE
  },
  { sequelize, modelName: 'News' }
)


