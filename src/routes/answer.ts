import { Express } from 'express'
import { AnswerController } from "../endpoints/_index";

export function routes(app: Express) {
  app.get('/api/answer', AnswerController.AnswerGet.list)
  app.post('/api/answer', AnswerController.AnswerPost.create)
  app.put('/api/answer', AnswerController.AnswerPut.update)
}
