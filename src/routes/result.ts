import { Express } from 'express'
import { ResultController } from "../endpoints/_index";

export function routes(app: Express) {
  app.get('/api/result', ResultController.ResultGet.list)
  app.get('/api/result/evaluation', ResultController.ResultGet.evaluation)
  app.post('/api/result', ResultController.ResultPost.create)
  app.post('/api/result/bySurveyId', ResultController.ResultPost.findBySurveyId)
  app.put('/api/result', ResultController.ResultPut.update)
}
