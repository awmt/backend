import * as winston from 'winston'
import { Express, Request, Response } from 'express'
import * as LanguagesRoutes from './languages'
import * as AppUserRoutes from './appusers'
import * as NewsRoutes from './news'
import * as SurveyRoutes from './survey'
import * as AnswerRoutes from './answer'
import * as ResultRoutes from './result'

export function initRoutes(app: Express) {
  winston.log('info', '--> Initialisations des routes')

  app.get('/api', (req: Request, res: Response) => res.status(200).send({
    message: 'server is running!'
  }))

  NewsRoutes.routes(app)
  SurveyRoutes.routes(app)
  AnswerRoutes.routes(app)
  ResultRoutes.routes(app)

  app.all('*', (req: Request, res: Response) => res.boom.notFound())
}
