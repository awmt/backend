import { Express } from 'express'
import { SurveyController } from "../endpoints/_index";

export function routes(app: Express) {
  app.get('/api/survey', SurveyController.SurveyGet.list)
  app.post('/api/survey', SurveyController.SurveyPost.create)
  app.post('/api/surveyWithAnswers', SurveyController.SurveyPost.withAnswers)
  app.put('/api/survey', SurveyController.SurveyPut.update)
}
