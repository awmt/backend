import { Express } from 'express'
import { NewsController } from '../endpoints/_index'

export function routes(app: Express) {

  app.get('/api/news', NewsController.NewsGet.list)
  app.post('/api/news', NewsController.NewsPost.create)
  app.post('/api/news/byDays', NewsController.NewsPost.findByDays)
  app.post('/api/news/count', NewsController.NewsPost.lastOnes)
  app.put('/api/news', NewsController.NewsPut.update)
}
