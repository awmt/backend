import * as uuid from 'uuid'
import { Result } from '../sqlz/models/result'
import { Survey } from '../sqlz/models/survey'
import { AnswerDao } from "../dao/_index";
import * as RelAnswerResult from './rel_answer_result'

export async function create(result: any): Promise<any> {
  try {
    const rEntry = await Result.create({
      id: uuid.v1(),
      surveyId: result.survey
    })
    for (const key in result.answers) {
      const answer = result.answers[key]
      const aEntry = await RelAnswerResult.create({ answer, result: rEntry.dataValues.id })
    }

    return rEntry;
  } catch (e) {
    console.log(e)
    return e
  }
}

export function update(result: any): Promise<any> {
  return Result
    .update({
      surveyId: result.survey
    }, {
      where: { id: result.id }
    })
}

export async function findAll(): Promise<any> {
  return Result
    .findAll()
}

export async function findBySurveyId(params: any): Promise<any> {
  const { survey } = params
  const results = await Result.findAll();
  let array = []

  for (const key in results) {
    const entry = results[key];
    if (entry.dataValues.surveyId === survey) {
      let resultWithAnswers = entry;
      resultWithAnswers.dataValues.answers = await RelAnswerResult.findAnswersByResultId({ id: entry.dataValues.id });

      array.push(resultWithAnswers);
    }
  }

  return array;
}

export async function currentResults(): Promise<any> {
  const surveys = await Survey.findAll();
  const currentSurvey = surveys[surveys.length - 1];
  const currentResults = await findBySurveyId({ survey: currentSurvey.dataValues.id });
  const answers = await AnswerDao.findBySurveyId(currentSurvey.dataValues.id);
  let evaluation: any = {};

  for (const key in answers) {
    const entry = answers[key].dataValues
    evaluation[entry.id] = { text: entry.content, count: 0 }
  }

  for (const key in currentResults) {
    const result = currentResults[key].dataValues;
    for (const key in result.answers) {
      const entry = result.answers[key].dataValues;
      evaluation[entry.id].count++;
    }
  }

  return { survey: currentSurvey.dataValues.id, participants: currentResults.length, evaluation };
}
