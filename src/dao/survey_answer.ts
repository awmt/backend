import * as uuid from "uuid";
import { SurveyAnswer } from "../sqlz/models/survey_answer";
import { getById } from "../endpoints/survey/survey.get";

export function create(answer: any): Promise<any> {
  return SurveyAnswer
    .create({
      id: uuid.v1(),
      content: answer.content,
      surveyId: answer.survey
    });
}

export function update(answer: any): Promise<any> {
  return SurveyAnswer
    .update({
      content: answer.content,
      surveyId: answer.survey
    }, {
      where: { id: answer.id }
    }
    )
}

export async function findAll(): Promise<any> {
  return SurveyAnswer
    .findAll()
}

export async function findById(params): Promise<any> {
  const { id } = params
  const answers = await SurveyAnswer.findAll();
  for (const key in answers) {
    const entry = answers[key]
    if (entry.dataValues.id === id) return entry;
  }

  return null;
}

export async function findWithSurvey(id: string): Promise<any> {
  const answers = await SurveyAnswer.findAll();
  let chosen = null;
  for (const key in answers) {
    let entry = answers[key];

    if (entry.id === id) {
      entry.dataValues.survey = await getById(entry.surveyId);
      chosen = entry
    }
  }

  return chosen;
}

export async function findBySurveyId(id: string): Promise<any> {
  const all = await SurveyAnswer.findAll();
  let chosen = [];

  for (const key in all) {
    const entry = all[key];
    if (entry.dataValues.surveyId === id) chosen.push(entry);
  }

  return chosen;
}
