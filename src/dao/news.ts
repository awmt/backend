import * as uuid from 'uuid'
import {News} from './../sqlz/models/news'

export function create(news: any): Promise<any> {
  return News
    .create({
      id: uuid.v1(),
      h1: news.h1,
      h2: news.h2,
      image: news.image,
      teaser: news.teaser,
      url: news.url,
      date: news.date,
      lastCrawled: news.lastCrawled
    })
}

export function update(news: any): Promise<any> {
  return News
    .update({
      h1: news.h1,
      h2: news.h2,
      image: news.image,
      teaser: news.teaser,
      url: news.url,
      date: news.date,
      lastCrawled: news.lastCrawled
    }, {
      where: {id: news.id}
    })
}

export function findAll(): Promise<any> {
  return News
    .findAll()
}

export async function findByDays(params: any): Promise<any> {
  const {days} = params
  const now = new Date(Date.now())
  const dateOffset = (24 * 60 * 60 * 1000) * days
  const chosenDate = new Date(now.getTime() - dateOffset)
  let chosenNews: News[] = []

  const allNews = await this.findAll()
  for (const key in allNews) {
    const entry = allNews[key]
    if (entry.date >= chosenDate) chosenNews.push(entry)
  }

  return chosenNews
}

export async function lastOnes(params: any): Promise<any> {
  const {count} = params
  let chosenNews: News[] = []

  return News
    .findAll({limit: count, order: [['date', 'DESC']]})
}
