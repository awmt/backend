import { RelAnswerResult } from "../sqlz/models/rel_answer_result";
import * as SurveyAnswer from "../dao/survey_answer";

export function create(rel: any): Promise<any> {
  return RelAnswerResult
    .create({
      answerId: rel.answer,
      resultId: rel.result
    })
}

export function update(rel: any): Promise<any> {
  return RelAnswerResult
    .update({
      answerId: rel.answer,
    }, {
      where: { resultId: rel.result }
    })
}

export async function findAnswersByResultId(params): Promise<any> {
  const { id } = params;
  const entries = await RelAnswerResult.findAll();
  let chosen = [];

  for (const key in entries) {
    const entry = entries[key].dataValues

    if (entry.resultId === id) {

      const answer = await SurveyAnswer.findById({ id: entry.answerId })
      chosen.push(answer)
    }
  }

  return chosen;
}

export async function findAll(): Promise<any> {
  return RelAnswerResult
    .findAll()
}
