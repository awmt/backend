import * as uuid from 'uuid'
import { Survey } from "../sqlz/models/survey";
import { getBySurveyId } from "../endpoints/answer/answer.get";

export function create(survey: any): Promise<any> {
  return Survey
    .create({
      id: uuid.v1(),
      question: survey.question,
      last_publication_date: survey.last_publication_date
    })
}

export function update(survey: any): Promise<any> {
  return Survey
    .update({
      question: survey.question
    }, {
      where: { id: survey.id }
    })
}

export async function findAll(): Promise<any> {
  return Survey
    .findAll()
}

export async function findWithAnswers(params: any): Promise<any> {
  const { id } = params
  const surveys = await Survey.findAll();
  let chosen = null;
  for (const key in surveys) {
    let entry = surveys[key];

    if (entry.dataValues.id === id) {
      entry.dataValues.answers = await getBySurveyId(entry.dataValues.id);
      chosen = entry
    }
  }

  return chosen;
}
