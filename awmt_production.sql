-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: mysql:3306
-- Erstellungszeit: 24. Mrz 2021 um 08:53
-- Server-Version: 5.7.32-35
-- PHP-Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `awmt_production`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `News`
--

CREATE TABLE `News` (
  `id` char(36) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `h2` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `teaser` varchar(50000) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `date` datetime DEFAULT NULL,
  `lastCrawled` datetime NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `News`
--

INSERT INTO `News` (`id`, `h1`, `h2`, `image`, `teaser`, `url`, `date`, `lastCrawled`, `createdAt`, `updatedAt`) VALUES
('3e9e3900-7f50-11eb-adbe-176943cd0f31', 'Wege zu einem nachhaltigen Finanzsystem', 'Beirat der Bundesregierung legt Empfehlungen vor', 'https://www.nabu.de/imperia/md/nabu/images/arten/tiere/insekten/schmetterlinge/tagfalter/140731-nabu-tagpfauenauge.jpeg', 'Der Abschlussbericht des Sustainable-Finance-Beirats liegt vor. Die Bundesregierung muss die Maßnahmen nun zügig umsetzen, um dem eigenen Anspruch gerecht zu werden, Deutschland zum führenden Standort für nachhaltige Finanzen zu machen.', 'https://www.nabu.de/news/2021/03/29514.html', '2021-03-03 00:00:00', '2021-03-22 22:35:54', '2021-03-07 15:20:18', '2021-03-22 21:35:54'),
('3e9fbfa0-7f50-11eb-adbe-176943cd0f31', 'Fragwürdiger Rettungsversuch für Butendiek', 'Meeresschutzgebiete müssen für Windparks tabu sein', 'https://www.nabu.de/imperia/md/nabu/images/umwelt/energie/energietraeger/windkraft/141204-nabu-offshore-windkraft-kim-dettloff.jpeg', 'Auf Aufforderung des Bundesamtes für Naturschutz (BfN) hat die Betreibergesellschaft wpd eine Ausnahmenprüfung für den Offshore-Windpark Butendiek beantragt. Nach Ansicht des NABU ist das ein fragwürdiger Winkelzug auf Kosten streng geschützter Seevögel. ', 'https://www.nabu.de/news/2021/03/29515.html', '2021-03-04 00:00:00', '2021-03-22 22:35:54', '2021-03-07 15:20:18', '2021-03-22 21:35:54'),
('61023bc0-8b46-11eb-bd03-658adc991195', 'Nach dem Frost ist vor dem Frost', 'Amphibien nutzen aktuelles mildes Wetter, doch es wird wieder kälter', 'https://www.nabu.de/imperia/md/nabu/images/arten/tiere/lurche/kroeten/141023-nabu-erdkroetenmaennchen-mit-brunftschwielen-karl-heinz-fuldner.jpeg', 'Wie in so vielen Jahren verläuft der Übergang vom Winter zum Frühjahr auch 2021 in Schüben. Momentan sind zwar in einigen Regionen Amphibien unterwegs. Spätestens zur Wochenmitte ist aber erneut eine Pause zu erwarten.', 'https://www.nabu.de/news/2021/03/kroetenwanderung.html', '2021-03-12 00:00:00', '2021-03-22 22:35:54', '2021-03-22 20:39:55', '2021-03-22 21:35:54'),
('61023bc1-8b46-11eb-bd03-658adc991195', 'Endspurt bei der Wahl zum Vogel des Jahres 2021 ', 'Abstimmen noch bis 19. März möglich - Sieger wird live verkündet', 'https://www.nabu.de/imperia/md/nabu/images/arten/tiere/voegel/schnaepperverwandte/rotkehlchen/160518-nabu-rotkehlchen-dorothea-bellmer.jpeg', 'Die erste öffentliche Wahl zum „Vogel des Jahres 2021“ ist auf der Zielgeraden. Unter den zehn Kandidaten der Stichwahl haben Rotkehlchen und Rauchschwalbe derzeit den Schnabel vorn. Auch Kiebitz und Feldlerche sind noch gut im Rennen. ', 'https://www.nabu.de/news/2021/03/29575.html', '2021-03-12 00:00:00', '2021-03-22 22:35:54', '2021-03-22 20:39:55', '2021-03-22 21:35:54'),
('610262d0-8b46-11eb-bd03-658adc991195', 'Entscheidung zu Windpark Butendiek', 'Enttäuschung auf ganzer Linie ', 'https://www.nabu.de/imperia/md/nabu/images/umwelt/energie/energietraeger/windkraft/141204-nabu-offshore-windkraft-kim-dettloff.jpeg', 'Das Oberverwaltungsgericht Münster hat über die Klage des NABU zur Sanierung des Umweltschadens durch den Offshore-Windpark Butendiek entschieden. Das Urteil setzt unüberwindbare Hürden für Umweltschadens- und Verbandsklagerecht.', 'https://www.nabu.de/news/2021/03/29579.html', '2021-03-12 00:00:00', '2021-03-22 22:35:54', '2021-03-22 20:39:55', '2021-03-22 21:35:54'),
('6102d800-8b46-11eb-bd03-658adc991195', 'Zum Weltwassertag', 'NABU fordert neue Gewässerpolitik', 'https://www.nabu.de/imperia/md/nabu/images/natur-landschaft/lebensraeume/kueste/watt/20210322-imagebroker-wattenmeer2.jpeg', 'Zum Weltwassertag macht der NABU auf das Missmanagement in der Gewässerpolitik aufmerksam. Probleme im Hamburger Hafen und Grubenwasser aus dem Erzgebirge gefährden Elbe und Wattenmeer. ', 'https://www.nabu.de/news/2021/03/29649.html', '2021-03-22 00:00:00', '2021-03-22 22:35:54', '2021-03-22 20:39:55', '2021-03-22 21:35:54'),
('6102ff10-8b46-11eb-bd03-658adc991195', 'NABU-Studie: Klöckners Agrarpläne widersprechen Green Deal', 'Bundesregierung riskiert Konflikt mit EU-Kommission', 'https://www.nabu.de/imperia/md/nabu/images/natur-landschaft/landnutzung/landwirtschaft/160920-nabu-heuwenden-favorit2-karkow.jpeg', 'Eine neue Studie im Auftrag des NABU zeigt, dass die geplante Umsetzung der EU-Agrarpolitik in Deutschland nicht mit den EU-Klima- und Artenschutzzielen vereinbar ist. Die Landesregierungen müssen jetzt auf Kurskorrektur pochen.', 'https://www.nabu.de/news/2021/03/29609.html', '2021-03-16 00:00:00', '2021-03-22 22:35:54', '2021-03-22 20:39:55', '2021-03-22 21:35:54'),
('61034d30-8b46-11eb-bd03-658adc991195', 'NABU und BMZ unterzeichnen „Kompetenzpartnerschaft Natur“', 'Gemeinsam für internationalen Waldschutz  ', 'https://www.nabu.de/imperia/md/nabu/images/international/afrika/aethiopien/kafa/landschaft/120116-nabu-nebelwald-kafa-bruno-d-amicis-680x453.jpeg', 'In Äthiopiens Kafa-Biosphärenreservat konnte die nachhaltige Bewirtschaftung von 10.000 Hektar Wald bereits gesichert werden. Jetzt sollen weitere erfolgreiche Projekte folgen. NABU, NABU International und BMZ unterzeichneten dafür eine „Kompetenzpartnerschaft Natur“. ', 'https://www.nabu.de/news/2021/03/29658.html', '2021-03-21 00:00:00', '2021-03-22 22:35:54', '2021-03-22 20:39:55', '2021-03-22 21:35:54'),
('61037440-8b46-11eb-bd03-658adc991195', 'Munitionsfunde beim Fehmarnbelttunnel', 'Schweinswalschutz muss bei Sprengungen Priorität haben', 'https://www.nabu.de/imperia/md/nabu/images/naturschutz/meeresschutz/191121-munitions-sprengungen-stefannehring.jpeg', 'Auf der dänischen Seite der Fehmarnbelt-Trasse sind Munitionsaltlasten gefunden worden, die am 23. März gesprengt werden sollen. Der Schweinswalschutz muss dabei oberste Priorität haben.', 'https://www.nabu.de/news/2021/03/29665.html', '2021-03-20 00:00:00', '2021-03-22 22:35:54', '2021-03-22 20:39:55', '2021-03-22 21:35:54'),
('61039b50-8b46-11eb-bd03-658adc991195', 'Vogel des Jahres 2021 gekürt', 'Rotkehlchen gewinnt erste öffentliche Wahl', 'https://www.nabu.de/imperia/md/nabu/images/projekte-aktionen/vogel-des-jahres/jubilaeum/top-ten/210319_vdj_gewinner_680x453.jpeg', 'Das Rotkehlchen ist der erste öffentlich gewählte Vogel des Jahres. Es hat mit 59.267 Stimmen vor Rauchschwalbe und Kiebitz das Rennen um den Titel gemacht. Insgesamt über 455.000 Menschen beteiligten sich an der Wahl. Das Rotkehlchen trägt nun zum zweiten Mal den Titel. ', 'https://www.nabu.de/news/2021/03/29654.html', '2021-03-19 00:00:00', '2021-03-22 22:35:54', '2021-03-22 20:39:55', '2021-03-22 21:35:54'),
('eece9ee0-7fec-11eb-adbe-176943cd0f31', 'Zuhause ist es doch am schönsten', '„Stunde der Wintervögel“: mehr Rotkehlchen und Spatzen, weniger Meisen', 'https://www.nabu.de/imperia/md/nabu/images/arten/tiere/voegel/schnaepperverwandte/rotkehlchen/160314-nabu-rotkehlchen-frank-derer.jpeg', 'Folgen des milden Winters: Während weniger Kälteflüchtlinge aus dem Norden und Osten zu uns kommen, bleiben immer mehr frostempfindliche Arten ganzjährig bei uns. Insgesamt wurden bei der „Stunde der Wintervögel“ 5,6 Millionen Vögel beobachtet, darunter 1,1 Millionen Hausperlinge.', 'https://www.nabu.de/news/2021/02/29347.html', '2021-02-03 00:00:00', '2021-03-08 11:01:55', '2021-03-08 10:01:56', '2021-03-08 10:01:56'),
('eed1d330-7fec-11eb-adbe-176943cd0f31', 'Erhöhtes Verkehrsaufkommen am Kamener Kreuz ', 'Die erste größere Etappe der Amphibienwanderungen ist voll im Gang', 'https://www.nabu.de/imperia/md/nabu/images/arten/tiere/lurche/froesche/141023-nabu-grasfroschpaare-mit-laich-rainer-pietsch.jpeg', 'Der starke Temperaturanstieg macht den Amphibien Beine. Von Tag zu Tag setzen sich in immer mehr Regionen Molche, Frösche und Kröten und Bewegung. Im Flachland, vor allem in den Flusstälern, sind inzwischen bundesweit Amphibien auf dem Weg zu den Laichgewässern.', 'https://www.nabu.de/news/2021/02/kroetenwanderung.html', '2021-02-26 00:00:00', '2021-03-08 11:01:55', '2021-03-08 10:01:56', '2021-03-08 10:01:56'),
('eed1fa40-7fec-11eb-adbe-176943cd0f31', 'Noch hat das Rotkehlchen den Schnabel vorn', 'Wahlkampfteams können noch viel Bewegung in die Wahl bringen', 'https://www.nabu.de/imperia/md/nabu/images/arten/tiere/voegel/schnaepperverwandte/rotkehlchen/20140813-nabu-rotkehlchen-thomas-munk.jpeg', 'Seit gut drei Wochen stimmen Vogelfreund*innen in ganz Deutschland für Ihren Kandidaten in der Wahl um den Vogel des Jahres 2021 ab. Es wurden mittlerweile schon über 190.000 Stimmen abgegeben. Die Rangliste der Top Ten verändert sich allerdings im Moment kaum.', 'https://www.nabu.de/news/2021/02/29401.html', '2021-02-10 00:00:00', '2021-03-08 11:01:55', '2021-03-08 10:01:56', '2021-03-08 10:01:56'),
('eed1fa41-7fec-11eb-adbe-176943cd0f31', 'Corona-Konjunkturpaket der EU startet', 'Bundesregierung muss Strategieplan nachbessern', 'https://www.nabu.de/imperia/md/nabu/images/gesellschaft/staat-politik/210218_eu_kommission_680x453.png', 'Nach der Einigung kurz vor Weihnachten, verteilt die EU nun die Gelder des Corona-Wiederaufbaufonds. Um Geld aus dem Fonds zu bekommen, müssen die Mitgliedstaaten nationale Reformpläne vorlegen. Doch grüne Investitionen gibt es im deutschen Plan nur wenige.', 'https://www.nabu.de/news/2021/02/29439.html', '2021-02-19 00:00:00', '2021-03-08 11:01:55', '2021-03-08 10:01:56', '2021-03-08 10:01:56'),
('eed22150-7fec-11eb-adbe-176943cd0f31', 'Gemeinsam Boden gut machen', 'Zehn Bio-Betriebe erhalten Förderpreis für Umstellung auf Ökolandbau', 'https://www.nabu.de/imperia/md/nabu/images/natur-landschaft/landnutzung/landwirtschaft/160704-nabu-heuwenden-helge-may3.jpeg', 'Der NABU hat zehn Landwirt*innen aus sechs Bundesländern für die vorbildliche Umstellung ihres landwirtschaftlichen Betriebs auf Ökolandbau mit dem Förderpreis „Gemeinsam Boden gut machen“ ausgezeichnet. ', 'https://www.nabu.de/news/2021/02/29424.html', '2021-02-18 00:00:00', '2021-03-08 11:01:55', '2021-03-08 10:01:56', '2021-03-08 10:01:56'),
('eed24860-7fec-11eb-adbe-176943cd0f31', 'Deutschland erhält Quittung für Nichtstun', 'Neue Naturschutzklage der EU gegen Deutschland', 'https://www.nabu.de/imperia/md/nabu/images/natur-landschaft/lebensraeume/moor/2020-11-30-pl__tzenseeflie__-biesenthaler_becken_restoration_peatland-_c_marc_sharping_nabu_680x453px.jpeg', 'Die Europäische Kommission hat heute beim Europäischen Gerichtshof gegen Deutschland Klage eingereicht. Sie wirft Bund und Ländern vor, die Schutzgebiete unzureichend rechtlich zu sichern und keine ausreichend konkreten Schutzziele zu formulieren. ', 'https://www.nabu.de/news/2021/02/29441.html', '2021-02-18 00:00:00', '2021-03-08 11:01:55', '2021-03-08 10:01:56', '2021-03-08 10:01:56'),
('f957eab0-7fec-11eb-adbe-176943cd0f31', 'Wann wandern die ersten Frösche, Molche und Kröten?', 'Mutige Amphibien nutzen auch kurze Wärmeeinbrüche', 'https://www.nabu.de/imperia/md/nabu/images/umwelt/verkehr/schilder-markierungen/180117-nabu-klappschild-helge-may.jpeg', 'Bald laufen sie wieder. Sobald die Nachttemperaturen bei plus fünf Grad Celsius und mehr liegen, kommen Frösche, Kröten und Molche in Hochzeitsstimmung. Wenn sie auf dem Weg zu den Laichgewässern Straßen überqueren müssen, können ganze Populationen den Verkehrstod erleiden. Um zu helfen, werden bundesweit Naturfreunde gesucht, die mit anpacken.', 'https://www.nabu.de/news/2021/01/kroetenwanderung.html', '2021-01-04 00:00:00', '2021-03-08 11:02:13', '2021-03-08 10:02:13', '2021-03-08 10:02:14'),
('f95886f0-7fec-11eb-adbe-176943cd0f31', 'Hurra, wir sind mehr als 200.000!', 'Die Stunde der Wintervögel schlägt alle Rekorde', 'https://www.nabu.de/imperia/md/nabu/images/arten/tiere/voegel/sperlinge/201216-nabu-haussperlingpaar-peter-trentz2.jpeg', 'Während Meisen, Kleiber oder Eichelhäher angesichts der vergleichsweise milden Witterung lieber im Wald bleiben, werden bei der aktuellen „Stunde der Wintervögel“ Haussperlinge und Rotkehlchen deutlich häufiger gezählt. Auch Ringeltauben lassen sich vermehrt blicken, ihre Bestände nehmen zu.', 'https://www.nabu.de/news/2021/01/sdw-ergebnisse.html', '2021-01-14 00:00:00', '2021-03-08 11:02:13', '2021-03-08 10:02:13', '2021-03-08 10:02:13'),
('f95c5780-7fec-11eb-adbe-176943cd0f31', 'Erhöhte Holznachfrage setzt Wälder unter Druck', 'Energetische Nutzung von Holz darf nicht gefördert werden', 'https://www.nabu.de/imperia/md/nabu/images/umwelt/energie/energietraeger/biomasse/190705-nabu-brennholzstapel-helge-may.jpeg', 'Umweltverbände rufen die Bundesregierung dazu auf, die weitere Expansion der industriellen Holzbiomasse-Energieproduktion nicht zu subventionieren. Holz in Kraftwerken zu verbrennen ist klimaschädlich und bedroht Waldökosysteme und Luftqualität.', 'https://www.nabu.de/news/2021/01/29236.html', '2020-01-13 00:00:00', '2021-03-08 11:02:13', '2021-03-08 10:02:13', '2021-03-08 10:02:13'),
('f95c7e90-7fec-11eb-adbe-176943cd0f31', 'Die Hauptwahl zum Vogel des Jahres startet', 'Zehn Kandidaten stehen zur Auswahl', 'https://www.nabu.de/imperia/md/nabu/images/projekte-aktionen/vogel-des-jahres/jubilaeum/top-ten/210114_nabu-vogelwahl-teaser_680x453.png', 'Ab sofort und noch bis zum 19. März können alle Menschen in Deutschland bestimmen, welcher der zehn nominierten Vögel das Rennen macht. Die Top Ten der Vorwahl kämpfen nun um den Titel „Vogel des Jahres 2021“. Stimmen Sie für Ihren Favoriten ab!', 'https://www.nabu.de/news/2021/01/29253.html', '2021-01-18 00:00:00', '2021-03-08 11:02:13', '2021-03-08 10:02:13', '2021-03-08 10:02:13'),
('f95ca5a0-7fec-11eb-adbe-176943cd0f31', 'Hohe Wahlbeteiligung, enges Rennen', 'Bei der Jahresvogelwahl liegen Rauchschwalbe und Rotkehlchen vorne', 'https://www.nabu.de/imperia/md/nabu/images/projekte-aktionen/vogel-des-jahres/jubilaeum/top-ten/210125-nabu-rotkehlchen-rauchschwalbe-frank-derer.jpeg', 'Wer folgt auf die Turteltaube? Innerhalb von zwei Wochen wurden bei der Stichwahl zum Vogel des Jahres 2021 bereits über 145.000 Stimmen abgegeben. Bei der ersten öffentlichen Wahl anlässlich des goldenen Jubiläums der Aktion ist das digitale Wahllokal noch bis zum 19. März geöffnet.', 'https://www.nabu.de/news/2021/01/29301.html', '2021-01-29 00:00:00', '2021-03-08 11:02:13', '2021-03-08 10:02:13', '2021-03-08 10:02:14'),
('f95d1ad0-7fec-11eb-adbe-176943cd0f31', '„Betonparagraph“ 13b streichen', 'Petition mit mehr als 35.000 Unterschriften übergeben', 'https://www.nabu.de/imperia/md/nabu/images/umwelt/siedlung-bauen/210127_flaechenfrass_gruppe_henning_koestler_680x453.png', 'Bei der Petition „Flächenfraß stoppen“ sprachen sich bislang mehr als 35.000 Menschen dafür aus, §13b des Baugesetzbuches zu streichen. Anlässlich der ersten Lesung übergab NABU-Präsident Jörg-Andreas Krüger den Zwischenstand an den stellvertretenden Vorsitzenden der SPD-Bundestagsfraktion Sören Bartol.', 'https://www.nabu.de/news/2021/01/29315.html', '2021-01-27 00:00:00', '2021-03-08 11:02:13', '2021-03-08 10:02:13', '2021-03-08 10:02:13'),
('f95d41e0-7fec-11eb-adbe-176943cd0f31', 'Gesetzentwurf zu Mehrweg muss ohne Ausnahmen gelten', 'Einführung der Mehrwegpflicht schützt Meere', 'https://www.nabu.de/imperia/md/nabu/images/umwelt/abfall/181008_einwegplastik_ericneuling.jpeg', 'Die im Entwurf des Verpackungsgesetzes vorgesehene Pflicht, neben Einweg auch Mehrweg für To-Go-Angebote bereitstellen zu müssen, ist ein Erfolg. Kritisch zu sehen sind die Ausnahmen, die im Vergleich zum Referentenentwurf aus dem November 2020 ausgeweitet wurden.', 'https://www.nabu.de/news/2021/01/29278.html', '2021-01-20 00:00:00', '2021-03-08 11:02:13', '2021-03-08 10:02:13', '2021-03-08 10:02:14');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `rel_answer_results`
--

CREATE TABLE `rel_answer_results` (
  `answerId` char(36) NOT NULL,
  `resultId` char(36) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `rel_answer_results`
--

INSERT INTO `rel_answer_results` (`answerId`, `resultId`, `createdAt`, `updatedAt`) VALUES
('2c195fd0-800e-11eb-adbe-176943cd0f31', '2b54a0d0-8af1-11eb-bd03-658adc991195', '2021-03-22 09:29:58', '2021-03-22 09:29:58'),
('2c195fd0-800e-11eb-adbe-176943cd0f31', '35ed40d0-8aef-11eb-bd03-658adc991195', '2021-03-22 09:15:57', '2021-03-22 09:15:57'),
('2c195fd0-800e-11eb-adbe-176943cd0f31', '5ed02da0-8b53-11eb-bd03-658adc991195', '2021-03-22 21:12:55', '2021-03-22 21:12:55'),
('2c195fd0-800e-11eb-adbe-176943cd0f31', '969bcf00-8112-11eb-adbe-176943cd0f31', '2021-03-09 20:04:00', '2021-03-09 20:04:00'),
('2c195fd0-800e-11eb-adbe-176943cd0f31', 'edb07870-810f-11eb-adbe-176943cd0f31', '2021-03-09 19:44:57', '2021-03-09 19:44:57'),
('2c2b8840-800e-11eb-adbe-176943cd0f31', '0f83c360-8112-11eb-adbe-176943cd0f31', '2021-03-09 20:00:13', '2021-03-09 20:00:13'),
('2c2b8840-800e-11eb-adbe-176943cd0f31', '21f45e80-8b5b-11eb-bd03-658adc991195', '2021-03-22 22:08:29', '2021-03-22 22:08:29'),
('2c2b8840-800e-11eb-adbe-176943cd0f31', '34ad2730-8bd5-11eb-bd03-658adc991195', '2021-03-23 12:42:19', '2021-03-23 12:42:19'),
('2c2b8840-800e-11eb-adbe-176943cd0f31', '528c80b0-8118-11eb-b4bc-4982a895f8b3', '2021-03-09 20:45:02', '2021-03-09 20:45:02'),
('2c2b8840-800e-11eb-adbe-176943cd0f31', '6db86e90-8112-11eb-adbe-176943cd0f31', '2021-03-09 20:02:51', '2021-03-09 20:02:51'),
('2c2b8840-800e-11eb-adbe-176943cd0f31', '71f12120-8110-11eb-adbe-176943cd0f31', '2021-03-09 19:48:39', '2021-03-09 19:48:39'),
('2c2b8840-800e-11eb-adbe-176943cd0f31', '9e5a3420-8c0c-11eb-b979-bba498be471c', '2021-03-23 19:18:58', '2021-03-23 19:18:58'),
('2c2b8840-800e-11eb-adbe-176943cd0f31', '9f395af0-8113-11eb-adbe-176943cd0f31', '2021-03-09 20:11:24', '2021-03-09 20:11:24'),
('2c2b8840-800e-11eb-adbe-176943cd0f31', 'a75dbd90-8bfc-11eb-b979-bba498be471c', '2021-03-23 17:24:42', '2021-03-23 17:24:42'),
('2c2b8840-800e-11eb-adbe-176943cd0f31', 'cb2bd020-8113-11eb-adbe-176943cd0f31', '2021-03-09 20:12:37', '2021-03-09 20:12:37'),
('2c2b8840-800e-11eb-adbe-176943cd0f31', 'e8e8b300-8116-11eb-ae7e-5f26c635b6ef', '2021-03-09 20:34:56', '2021-03-09 20:34:56'),
('2c2b8840-800e-11eb-adbe-176943cd0f31', 'edb07870-810f-11eb-adbe-176943cd0f31', '2021-03-09 19:44:57', '2021-03-09 19:44:57'),
('2c3f1040-800e-11eb-adbe-176943cd0f31', '0f83c360-8112-11eb-adbe-176943cd0f31', '2021-03-09 20:00:13', '2021-03-09 20:00:13'),
('2c3f1040-800e-11eb-adbe-176943cd0f31', '21f45e80-8b5b-11eb-bd03-658adc991195', '2021-03-22 22:08:29', '2021-03-22 22:08:29'),
('2c3f1040-800e-11eb-adbe-176943cd0f31', '34ad2730-8bd5-11eb-bd03-658adc991195', '2021-03-23 12:42:19', '2021-03-23 12:42:19'),
('2c3f1040-800e-11eb-adbe-176943cd0f31', '35ed40d0-8aef-11eb-bd03-658adc991195', '2021-03-22 09:15:57', '2021-03-22 09:15:57'),
('2c3f1040-800e-11eb-adbe-176943cd0f31', '3adb15c0-8af1-11eb-bd03-658adc991195', '2021-03-22 09:30:24', '2021-03-22 09:30:24'),
('2c3f1040-800e-11eb-adbe-176943cd0f31', '45f957f0-8114-11eb-adbe-176943cd0f31', '2021-03-09 20:16:04', '2021-03-09 20:16:04'),
('2c3f1040-800e-11eb-adbe-176943cd0f31', '55bb6ef0-8117-11eb-ae7e-5f26c635b6ef', '2021-03-09 20:37:58', '2021-03-09 20:37:58'),
('2c3f1040-800e-11eb-adbe-176943cd0f31', '71f12120-8110-11eb-adbe-176943cd0f31', '2021-03-09 19:48:39', '2021-03-09 19:48:39'),
('2c3f1040-800e-11eb-adbe-176943cd0f31', '78257110-8118-11eb-9aea-35c6dbe3b96b', '2021-03-09 20:46:06', '2021-03-09 20:46:06'),
('2c3f1040-800e-11eb-adbe-176943cd0f31', '7bce2000-8113-11eb-adbe-176943cd0f31', '2021-03-09 20:10:24', '2021-03-09 20:10:24'),
('2c3f1040-800e-11eb-adbe-176943cd0f31', '9f02e900-8110-11eb-adbe-176943cd0f31', '2021-03-09 19:49:55', '2021-03-09 19:49:55'),
('2c3f1040-800e-11eb-adbe-176943cd0f31', 'a346aff0-8b52-11eb-bd03-658adc991195', '2021-03-22 21:07:40', '2021-03-22 21:07:40'),
('2c3f1040-800e-11eb-adbe-176943cd0f31', 'e8e8b300-8116-11eb-ae7e-5f26c635b6ef', '2021-03-09 20:34:56', '2021-03-09 20:34:56'),
('2c50c380-800e-11eb-adbe-176943cd0f31', '38d96640-8115-11eb-adbe-176943cd0f31', '2021-03-09 20:22:51', '2021-03-09 20:22:51'),
('2c50c380-800e-11eb-adbe-176943cd0f31', '3addaf60-8b55-11eb-bd03-658adc991195', '2021-03-22 21:26:14', '2021-03-22 21:26:14'),
('2c50c380-800e-11eb-adbe-176943cd0f31', '45f957f0-8114-11eb-adbe-176943cd0f31', '2021-03-09 20:16:04', '2021-03-09 20:16:04'),
('2c50c380-800e-11eb-adbe-176943cd0f31', '5ed02da0-8b53-11eb-bd03-658adc991195', '2021-03-22 21:12:55', '2021-03-22 21:12:55'),
('2c50c380-800e-11eb-adbe-176943cd0f31', '78257110-8118-11eb-9aea-35c6dbe3b96b', '2021-03-09 20:46:06', '2021-03-09 20:46:06'),
('2c50c380-800e-11eb-adbe-176943cd0f31', '9f02e900-8110-11eb-adbe-176943cd0f31', '2021-03-09 19:49:55', '2021-03-09 19:49:55'),
('2c50c380-800e-11eb-adbe-176943cd0f31', '9feb4900-8117-11eb-ae7e-5f26c635b6ef', '2021-03-09 20:40:03', '2021-03-09 20:40:03'),
('2c50c380-800e-11eb-adbe-176943cd0f31', 'a75dbd90-8bfc-11eb-b979-bba498be471c', '2021-03-23 17:24:42', '2021-03-23 17:24:42'),
('2c50c380-800e-11eb-adbe-176943cd0f31', 'd45b9f10-8116-11eb-ae7e-5f26c635b6ef', '2021-03-09 20:34:21', '2021-03-09 20:34:21'),
('2c50c380-800e-11eb-adbe-176943cd0f31', 'e1014d20-8114-11eb-adbe-176943cd0f31', '2021-03-09 20:20:23', '2021-03-09 20:20:23'),
('2c5cd170-800e-11eb-adbe-176943cd0f31', '21f45e80-8b5b-11eb-bd03-658adc991195', '2021-03-22 22:08:29', '2021-03-22 22:08:29'),
('2c5cd170-800e-11eb-adbe-176943cd0f31', '928bdd50-8118-11eb-9aea-35c6dbe3b96b', '2021-03-09 20:46:50', '2021-03-09 20:46:50'),
('2c5cd170-800e-11eb-adbe-176943cd0f31', '9edffae0-8114-11eb-adbe-176943cd0f31', '2021-03-09 20:18:33', '2021-03-09 20:18:33'),
('2c5cd170-800e-11eb-adbe-176943cd0f31', 'd1be70e0-8114-11eb-adbe-176943cd0f31', '2021-03-09 20:19:58', '2021-03-09 20:19:58'),
('2c684320-800e-11eb-adbe-176943cd0f31', '108736f0-8b55-11eb-bd03-658adc991195', '2021-03-22 21:25:03', '2021-03-22 21:25:03'),
('2c684320-800e-11eb-adbe-176943cd0f31', '29e3ca30-8116-11eb-adbe-176943cd0f31', '2021-03-09 20:29:35', '2021-03-09 20:29:35'),
('2c684320-800e-11eb-adbe-176943cd0f31', '407c1440-8112-11eb-adbe-176943cd0f31', '2021-03-09 20:01:35', '2021-03-09 20:01:35'),
('2c684320-800e-11eb-adbe-176943cd0f31', '799d2450-8115-11eb-adbe-176943cd0f31', '2021-03-09 20:24:39', '2021-03-09 20:24:39'),
('2c684320-800e-11eb-adbe-176943cd0f31', 'd58631d0-8115-11eb-adbe-176943cd0f31', '2021-03-09 20:27:14', '2021-03-09 20:27:14');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Results`
--

CREATE TABLE `Results` (
  `id` char(36) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `surveyId` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `Results`
--

INSERT INTO `Results` (`id`, `createdAt`, `updatedAt`, `surveyId`) VALUES
('0f83c360-8112-11eb-adbe-176943cd0f31', '2021-03-09 20:00:13', '2021-03-09 20:00:13', '2c025560-800e-11eb-adbe-176943cd0f31'),
('108736f0-8b55-11eb-bd03-658adc991195', '2021-03-22 21:25:03', '2021-03-22 21:25:03', '2c025560-800e-11eb-adbe-176943cd0f31'),
('21f45e80-8b5b-11eb-bd03-658adc991195', '2021-03-22 22:08:29', '2021-03-22 22:08:29', '2c025560-800e-11eb-adbe-176943cd0f31'),
('29e3ca30-8116-11eb-adbe-176943cd0f31', '2021-03-09 20:29:35', '2021-03-09 20:29:35', '2c025560-800e-11eb-adbe-176943cd0f31'),
('2b54a0d0-8af1-11eb-bd03-658adc991195', '2021-03-22 09:29:58', '2021-03-22 09:29:58', '2c025560-800e-11eb-adbe-176943cd0f31'),
('34ad2730-8bd5-11eb-bd03-658adc991195', '2021-03-23 12:42:19', '2021-03-23 12:42:19', '2c025560-800e-11eb-adbe-176943cd0f31'),
('35ed40d0-8aef-11eb-bd03-658adc991195', '2021-03-22 09:15:57', '2021-03-22 09:15:57', '2c025560-800e-11eb-adbe-176943cd0f31'),
('38d96640-8115-11eb-adbe-176943cd0f31', '2021-03-09 20:22:51', '2021-03-09 20:22:51', '2c025560-800e-11eb-adbe-176943cd0f31'),
('3adb15c0-8af1-11eb-bd03-658adc991195', '2021-03-22 09:30:24', '2021-03-22 09:30:24', '2c025560-800e-11eb-adbe-176943cd0f31'),
('3addaf60-8b55-11eb-bd03-658adc991195', '2021-03-22 21:26:14', '2021-03-22 21:26:14', '2c025560-800e-11eb-adbe-176943cd0f31'),
('407c1440-8112-11eb-adbe-176943cd0f31', '2021-03-09 20:01:35', '2021-03-09 20:01:35', '2c025560-800e-11eb-adbe-176943cd0f31'),
('45f957f0-8114-11eb-adbe-176943cd0f31', '2021-03-09 20:16:03', '2021-03-09 20:16:03', '2c025560-800e-11eb-adbe-176943cd0f31'),
('528c80b0-8118-11eb-b4bc-4982a895f8b3', '2021-03-09 20:45:02', '2021-03-09 20:45:02', '2c025560-800e-11eb-adbe-176943cd0f31'),
('55bb6ef0-8117-11eb-ae7e-5f26c635b6ef', '2021-03-09 20:37:58', '2021-03-09 20:37:58', '2c025560-800e-11eb-adbe-176943cd0f31'),
('5ed02da0-8b53-11eb-bd03-658adc991195', '2021-03-22 21:12:55', '2021-03-22 21:12:55', '2c025560-800e-11eb-adbe-176943cd0f31'),
('6db86e90-8112-11eb-adbe-176943cd0f31', '2021-03-09 20:02:51', '2021-03-09 20:02:51', '2c025560-800e-11eb-adbe-176943cd0f31'),
('71f12120-8110-11eb-adbe-176943cd0f31', '2021-03-09 19:48:39', '2021-03-09 19:48:39', '2c025560-800e-11eb-adbe-176943cd0f31'),
('78257110-8118-11eb-9aea-35c6dbe3b96b', '2021-03-09 20:46:05', '2021-03-09 20:46:05', '2c025560-800e-11eb-adbe-176943cd0f31'),
('799d2450-8115-11eb-adbe-176943cd0f31', '2021-03-09 20:24:39', '2021-03-09 20:24:39', '2c025560-800e-11eb-adbe-176943cd0f31'),
('7bce2000-8113-11eb-adbe-176943cd0f31', '2021-03-09 20:10:24', '2021-03-09 20:10:24', '2c025560-800e-11eb-adbe-176943cd0f31'),
('928bdd50-8118-11eb-9aea-35c6dbe3b96b', '2021-03-09 20:46:50', '2021-03-09 20:46:50', '2c025560-800e-11eb-adbe-176943cd0f31'),
('969bcf00-8112-11eb-adbe-176943cd0f31', '2021-03-09 20:04:00', '2021-03-09 20:04:00', '2c025560-800e-11eb-adbe-176943cd0f31'),
('9e5a3420-8c0c-11eb-b979-bba498be471c', '2021-03-23 19:18:58', '2021-03-23 19:18:58', '2c025560-800e-11eb-adbe-176943cd0f31'),
('9edffae0-8114-11eb-adbe-176943cd0f31', '2021-03-09 20:18:32', '2021-03-09 20:18:32', '2c025560-800e-11eb-adbe-176943cd0f31'),
('9f02e900-8110-11eb-adbe-176943cd0f31', '2021-03-09 19:49:55', '2021-03-09 19:49:55', '2c025560-800e-11eb-adbe-176943cd0f31'),
('9f395af0-8113-11eb-adbe-176943cd0f31', '2021-03-09 20:11:24', '2021-03-09 20:11:24', '2c025560-800e-11eb-adbe-176943cd0f31'),
('9feb4900-8117-11eb-ae7e-5f26c635b6ef', '2021-03-09 20:40:03', '2021-03-09 20:40:03', '2c025560-800e-11eb-adbe-176943cd0f31'),
('a346aff0-8b52-11eb-bd03-658adc991195', '2021-03-22 21:07:40', '2021-03-22 21:07:40', '2c025560-800e-11eb-adbe-176943cd0f31'),
('a75dbd90-8bfc-11eb-b979-bba498be471c', '2021-03-23 17:24:42', '2021-03-23 17:24:42', '2c025560-800e-11eb-adbe-176943cd0f31'),
('cb2bd020-8113-11eb-adbe-176943cd0f31', '2021-03-09 20:12:37', '2021-03-09 20:12:37', '2c025560-800e-11eb-adbe-176943cd0f31'),
('d1be70e0-8114-11eb-adbe-176943cd0f31', '2021-03-09 20:19:58', '2021-03-09 20:19:58', '2c025560-800e-11eb-adbe-176943cd0f31'),
('d45b9f10-8116-11eb-ae7e-5f26c635b6ef', '2021-03-09 20:34:21', '2021-03-09 20:34:21', '2c025560-800e-11eb-adbe-176943cd0f31'),
('d58631d0-8115-11eb-adbe-176943cd0f31', '2021-03-09 20:27:14', '2021-03-09 20:27:14', '2c025560-800e-11eb-adbe-176943cd0f31'),
('e1014d20-8114-11eb-adbe-176943cd0f31', '2021-03-09 20:20:23', '2021-03-09 20:20:23', '2c025560-800e-11eb-adbe-176943cd0f31'),
('e8e8b300-8116-11eb-ae7e-5f26c635b6ef', '2021-03-09 20:34:56', '2021-03-09 20:34:56', '2c025560-800e-11eb-adbe-176943cd0f31'),
('edb07870-810f-11eb-adbe-176943cd0f31', '2021-03-09 19:44:57', '2021-03-09 19:44:57', '2c025560-800e-11eb-adbe-176943cd0f31');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `SequelizeMeta`
--

CREATE TABLE `SequelizeMeta` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Surveys`
--

CREATE TABLE `Surveys` (
  `id` char(36) NOT NULL,
  `question` text,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_publication_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `Surveys`
--

INSERT INTO `Surveys` (`id`, `question`, `createdAt`, `updatedAt`, `last_publication_date`) VALUES
('2c025560-800e-11eb-adbe-176943cd0f31', 'Wie informieren Sie sich hauptsächlich über politische Entwicklungen?', '2021-03-08 12:59:52', '2021-03-08 12:59:52', '2021-03-07');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `Survey_answers`
--

CREATE TABLE `Survey_answers` (
  `id` char(36) NOT NULL,
  `content` text,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `surveyId` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `Survey_answers`
--

INSERT INTO `Survey_answers` (`id`, `content`, `createdAt`, `updatedAt`, `surveyId`) VALUES
('2c195fd0-800e-11eb-adbe-176943cd0f31', 'Tageszeitung (print oder online)', '2021-03-08 12:59:52', '2021-03-08 12:59:52', '2c025560-800e-11eb-adbe-176943cd0f31'),
('2c2b8840-800e-11eb-adbe-176943cd0f31', 'TV', '2021-03-08 12:59:52', '2021-03-08 12:59:52', '2c025560-800e-11eb-adbe-176943cd0f31'),
('2c3f1040-800e-11eb-adbe-176943cd0f31', 'Radio', '2021-03-08 12:59:52', '2021-03-08 12:59:52', '2c025560-800e-11eb-adbe-176943cd0f31'),
('2c50c380-800e-11eb-adbe-176943cd0f31', 'Soziale Medien', '2021-03-08 12:59:52', '2021-03-08 12:59:52', '2c025560-800e-11eb-adbe-176943cd0f31'),
('2c5cd170-800e-11eb-adbe-176943cd0f31', 'Andere', '2021-03-08 12:59:52', '2021-03-08 12:59:52', '2c025560-800e-11eb-adbe-176943cd0f31'),
('2c684320-800e-11eb-adbe-176943cd0f31', 'Gar nicht', '2021-03-08 12:59:52', '2021-03-08 12:59:52', '2c025560-800e-11eb-adbe-176943cd0f31');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `News`
--
ALTER TABLE `News`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `rel_answer_results`
--
ALTER TABLE `rel_answer_results`
  ADD PRIMARY KEY (`answerId`,`resultId`),
  ADD KEY `result` (`resultId`);

--
-- Indizes für die Tabelle `Results`
--
ALTER TABLE `Results`
  ADD PRIMARY KEY (`id`),
  ADD KEY `survey` (`surveyId`) USING BTREE;

--
-- Indizes für die Tabelle `SequelizeMeta`
--
ALTER TABLE `SequelizeMeta`
  ADD PRIMARY KEY (`name`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indizes für die Tabelle `Surveys`
--
ALTER TABLE `Surveys`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `last_publication_date` (`last_publication_date`);

--
-- Indizes für die Tabelle `Survey_answers`
--
ALTER TABLE `Survey_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `survey` (`surveyId`);

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `rel_answer_results`
--
ALTER TABLE `rel_answer_results`
  ADD CONSTRAINT `rel_answer_results_ibfk_1` FOREIGN KEY (`answerId`) REFERENCES `Survey_answers` (`id`),
  ADD CONSTRAINT `rel_answer_results_ibfk_2` FOREIGN KEY (`resultId`) REFERENCES `Results` (`id`);

--
-- Constraints der Tabelle `Results`
--
ALTER TABLE `Results`
  ADD CONSTRAINT `Results_ibfk_1` FOREIGN KEY (`surveyId`) REFERENCES `Surveys` (`id`);

--
-- Constraints der Tabelle `Survey_answers`
--
ALTER TABLE `Survey_answers`
  ADD CONSTRAINT `Survey_answers_ibfk_1` FOREIGN KEY (`surveyId`) REFERENCES `Surveys` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
